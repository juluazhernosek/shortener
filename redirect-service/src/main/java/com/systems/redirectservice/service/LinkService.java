package com.systems.redirectservice.service;


import com.systems.redirectservice.domain.Link;

public interface LinkService {
    Link getLink(String shortLink);
}
