package com.systems.redirectservice.service.impl;

import com.systems.redirectservice.domain.Link;
import com.systems.redirectservice.repository.LinkRepository;
import com.systems.redirectservice.service.LinkService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LinkServiceImpl implements LinkService {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private LinkRepository linkRepository;

    @Override
    public Link getLink(String shortLink) {
        Link link = linkRepository.findOne(shortLink);
        updateStatistic(link);
        return link;
    }

    private void updateStatistic(Link link) {
        if(link != null)
            rabbitTemplate.convertAndSend(link.getShortLink());
    }
}
