package com.systems.redirectservice.controller;

import com.systems.redirectservice.domain.Link;
import com.systems.redirectservice.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedirectController {

    @Autowired
    private LinkService linkService;

    @RequestMapping("/{link}")
    public ResponseEntity<String> redirect(@PathVariable("link") String shortLink) {
        return createResponse(linkService.getLink(shortLink));
    }

    private ResponseEntity<String> createResponse(Link link) {
        if (link == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, link.getLink());
        return new ResponseEntity<>(headers, HttpStatus.valueOf(link.getRedirectCode()));
    }
}
