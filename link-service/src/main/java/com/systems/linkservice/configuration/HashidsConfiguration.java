package com.systems.linkservice.configuration;

import org.hashids.Hashids;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HashidsConfiguration {

    private static final String SALT = "";
    private static final int MIN_HASH_LENGTH = 6;

    @Bean
    public Hashids hashids() {
        return new Hashids(SALT, MIN_HASH_LENGTH);
    }
}
