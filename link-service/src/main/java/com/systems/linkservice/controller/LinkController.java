package com.systems.linkservice.controller;

import com.systems.linkservice.domain.Link;
import com.systems.linkservice.dto.RegisterRequestDto;
import com.systems.linkservice.dto.RegisterResponseDto;
import com.systems.linkservice.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(path = "/")
public class LinkController {

    @Autowired
    private LinkService linkService;

    @RequestMapping(method = RequestMethod.POST)
    public RegisterResponseDto createShortLink(@RequestBody RegisterRequestDto registerRequestDto, Principal principal,
                                               @Value("${application.domain}") String domain) {
        Link link = linkService.createLink(registerRequestDto.getUrl(), registerRequestDto.getRedirectType(), principal.getName());
        RegisterResponseDto registerResponseDto = new RegisterResponseDto();
        registerResponseDto.setShortUrl(domain + link.getShortLink());
        return registerResponseDto;
    }
}
