package com.systems.linkservice.service;


import com.systems.linkservice.domain.Link;

public interface LinkService {
    Link createLink(String longLink, Integer redirectCode, String username);
}
