package com.systems.linkservice.service.impl;

import com.systems.linkservice.domain.Link;
import com.systems.linkservice.dto.LinkQueue;
import com.systems.linkservice.repository.LinkRepository;
import com.systems.linkservice.service.LinkService;
import com.systems.linkservice.util.UniqueShortLinkUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class LinkServiceImpl implements LinkService {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private UniqueShortLinkUtil uniqueShortLinkUtil;
    @Autowired
    private LinkRepository linkRepository;

    @Override
    public Link createLink(String longLink, Integer redirectCode, String username) {
        Link link = new Link();
        link.setShortLink(uniqueShortLinkUtil.getUniqueShortLink());
        link.setLink(longLink);
        link.setRedirectCode(redirectCode);
        linkRepository.save(link);
        updateStatistic(link, username);
        return link;
    }

    private void updateStatistic(Link link, String username) {
        LinkQueue linkQueue = new LinkQueue();
        linkQueue.setLink(link.getLink());
        linkQueue.setShortLink(link.getShortLink());
        linkQueue.setUsername(username);
        rabbitTemplate.convertAndSend(linkQueue);
    }
}
