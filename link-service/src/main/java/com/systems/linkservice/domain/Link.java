package com.systems.linkservice.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("links")
public class Link {
    @Id
    private String shortLink;
    private String link;
    private Integer redirectCode;

    public String getShortLink() {
        return shortLink;
    }

    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getRedirectCode() {
        return redirectCode;
    }

    public void setRedirectCode(Integer redirectCode) {
        this.redirectCode = redirectCode;
    }
}
