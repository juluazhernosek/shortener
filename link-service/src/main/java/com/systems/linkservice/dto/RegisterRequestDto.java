package com.systems.linkservice.dto;

public class RegisterRequestDto {
    private String url;
    private Integer redirectType;

    public RegisterRequestDto() {
        redirectType = 302;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(Integer redirectType) {
        this.redirectType = redirectType;
    }
}
