package com.systems.linkservice.util;

public interface UniqueShortLinkUtil {
    String getUniqueShortLink();
}
