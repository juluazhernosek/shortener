package com.systems.linkservice.util.impl;

import com.systems.linkservice.util.UniqueShortLinkUtil;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

@Component
public class UniqueShortLinkUtilImpl implements UniqueShortLinkUtil {

    @Autowired
    private RedisAtomicLong redisSequenceId;

    @Autowired
    private Hashids hashids;

    @Override
    public String getUniqueShortLink() {
        return hashids.encode(redisSequenceId.incrementAndGet());
    }
}
