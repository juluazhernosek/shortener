package com.systems.accountservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreationAccountRequest {
    @JsonProperty("AccountId")
    private String accountId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
