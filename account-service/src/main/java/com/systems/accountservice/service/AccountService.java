package com.systems.accountservice.service;

import com.systems.accountservice.domain.Account;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface AccountService extends UserDetailsService {
    Account create(Account account);
}
