package com.systems.accountservice.service.security;

import com.systems.accountservice.domain.Account;
import com.systems.accountservice.repository.AccountRepository;
import com.systems.accountservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findOne(username);
        if (account == null)
            throw new UsernameNotFoundException(username);
        return account;
    }

    @Override
    public Account create(Account account) {
        return accountRepository.insert(account);
    }
}
