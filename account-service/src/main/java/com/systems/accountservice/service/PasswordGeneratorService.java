package com.systems.accountservice.service;

public interface PasswordGeneratorService {
    String generatePassword();
}
