package com.systems.accountservice.service.impl;

import com.systems.accountservice.service.PasswordGeneratorService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class PasswordGeneratorServiceImpl implements PasswordGeneratorService {
    @Override
    public String generatePassword() {
        return RandomStringUtils.randomAlphabetic(8);
    }
}
