package com.systems.accountservice.controller;

import com.systems.accountservice.domain.Account;
import com.systems.accountservice.dto.AccountCreationResponse;
import com.systems.accountservice.dto.CreationAccountRequest;
import com.systems.accountservice.service.AccountService;
import com.systems.accountservice.service.PasswordGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private PasswordGeneratorService passwordGeneratorService;

    @RequestMapping(method = RequestMethod.POST)
    public AccountCreationResponse createAccount(@RequestBody CreationAccountRequest creationAccountRequest) {
        Account account = new Account();
        account.setUsername(creationAccountRequest.getAccountId());
        account.setPassword(passwordGeneratorService.generatePassword());
        return response(accountService.create(account));
    }

    @RequestMapping(path = "/current", method = RequestMethod.GET)
    public Authentication currentAccount(Authentication principal) {
        return principal;
    }

    private AccountCreationResponse response(Account account) {
        AccountCreationResponse accountCreationResponse = new AccountCreationResponse();
        accountCreationResponse.setPassword(account.getPassword());
        accountCreationResponse.setSuccess(true);
        accountCreationResponse.setDescription("Your account is opened");
        return accountCreationResponse;
    }
}
