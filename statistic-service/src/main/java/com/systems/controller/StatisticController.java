package com.systems.controller;

import com.systems.domain.Statistic;
import com.systems.service.StatisticService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController("/statistic")
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public Map<String, Integer> getStatisticForUser(@PathVariable("username") String username) {
        List<Statistic> statisticForUser = statisticService.getStatisticForUser(username);
        return statisticForUser.stream().collect(Collectors.toMap(Statistic::getLink, Statistic::getRedirectCount, Integer::sum));
    }
}
