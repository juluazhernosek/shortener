package com.systems.listener;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.systems.domain.Statistic;
import com.systems.service.StatisticService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;

@Component
public class StatisticQueueListener implements MessageListener {

    private static final Logger LOG = LoggerFactory.getLogger(StatisticQueueListener.class);

    @Autowired
    private StatisticService statisticService;
    @Autowired
    private ObjectMapper jsonParser;

    @Override
    public void onMessage(Message message) {
        LOG.debug("Incoming message: {}", new String(message.getBody()));
        try {
            Statistic statistic = jsonParser.readValue(message.getBody(), Statistic.class);
            statisticService.updateStatistic(statistic);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }

    }
}
