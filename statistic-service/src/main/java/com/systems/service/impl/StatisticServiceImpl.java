package com.systems.service.impl;

import com.systems.domain.Statistic;
import com.systems.repository.StatisticCustomRepository;
import com.systems.repository.StatisticRepository;
import com.systems.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StatisticServiceImpl implements StatisticService {

    @Autowired
    private StatisticCustomRepository statisticCustomRepository;
    @Autowired
    private StatisticRepository statisticRepository;

    @Override
    public void updateStatistic(Statistic statistic) {
        statisticCustomRepository.incrementRedirectCountByShortLink(statistic);
    }

    @Override
    public List<Statistic> getStatisticForUser(String username) {
        return statisticRepository.findByUsername(username);
    }
}
