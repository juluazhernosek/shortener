package com.systems.service;


import com.systems.domain.Statistic;

import java.util.List;

public interface StatisticService {
    void updateStatistic(Statistic statistic);
    List<Statistic> getStatisticForUser(String username);
}
