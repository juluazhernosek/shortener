package com.systems.repository;

import com.systems.domain.Statistic;

public interface StatisticCustomRepository {
    void incrementRedirectCountByShortLink(Statistic statistic);
}
