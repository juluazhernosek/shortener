package com.systems.repository.impl;

import com.systems.domain.Statistic;
import com.systems.repository.StatisticCustomRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.keyvalue.core.KeyValueOperations;
import org.springframework.data.redis.core.PartialUpdate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StatisticCustomRepositoryImpl implements StatisticCustomRepository {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private KeyValueOperations keyValueOperations;

    @Override
    public void incrementRedirectCountByShortLink(Statistic statistic) {
        try {
            keyValueOperations.insert(statistic);
        } catch (DuplicateKeyException e) {
            String keyShortLink = "statistic:" + statistic.getShortLink();
            redisTemplate.opsForHash().increment(keyShortLink, "redirectCount", 1);
            if(statistic.getUsername() != null) {
                keyValueOperations.update(new PartialUpdate<>(statistic.getShortLink(), Statistic.class)
                        .set("username", statistic.getUsername()));
            }
        }
    }
}
