package com.systems.repository;

import com.systems.domain.Statistic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatisticRepository extends CrudRepository<Statistic, String> {
    List<Statistic> findByUsername(String username);
}
