package com.systems;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class StatisticServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(StatisticServiceApplication.class, args);
    }

    @Bean
    public ObjectMapper jsonParser() {
        return new ObjectMapper();
    }
}
