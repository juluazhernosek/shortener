package com.systems.gatewayservice;

import com.systems.gatewayservice.security.provider.AccountAuthenticationProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
@EnableWebSecurity
@EnableFeignClients
@EnableWebMvc
public class GatewayApplication extends WebSecurityConfigurerAdapter {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/account/current").denyAll()
                .antMatchers("/account").permitAll()
                .antMatchers("/statistic/**").authenticated()
                .antMatchers("/register").authenticated()
                .anyRequest().permitAll()
                .and()
                .csrf().disable()
                .authenticationProvider(authenticationProvider())
                .httpBasic();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new AccountAuthenticationProvider();
    }

    @Configuration
    public static class WebConfiguration extends WebMvcConfigurerAdapter {
        @Bean
        public InternalResourceViewResolver viewResolver() {
            InternalResourceViewResolver resolver = new InternalResourceViewResolver();
            resolver.setPrefix("/apidoc/");
            resolver.setSuffix(".html");
            return resolver;
        }
        @Override
        public void addViewControllers(ViewControllerRegistry registry) {
            registry.setOrder(-202);
            registry.addViewController("/help").setViewName("index");
        }

        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.setOrder(-201);
            registry.addResourceHandler("/apidoc/**").addResourceLocations("classpath:/apidoc/");
            registry.addResourceHandler("/api/**").addResourceLocations("classpath:/api/");
            registry.addResourceHandler("/help/api/**").addResourceLocations("classpath:/api/");
            registry.addResourceHandler("/images/**").addResourceLocations("classpath:/apidoc/images/");
            registry.addResourceHandler("/css/**").addResourceLocations("classpath:/apidoc/css/");
            registry.addResourceHandler("/lib/**").addResourceLocations("classpath:/apidoc/lib/");
            registry.addResourceHandler("/core/**").addResourceLocations("classpath:/apidoc/core/");
            registry.addResourceHandler("/help").addResourceLocations("classpath:/apidoc/");

        }
    }
}
