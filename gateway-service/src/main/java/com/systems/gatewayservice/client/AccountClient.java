package com.systems.gatewayservice.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "account-service")
public interface AccountClient {
    @RequestMapping(method = RequestMethod.GET, value = "/account/current", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void currentAccount(@RequestHeader("Authorization") String authorization);
}
