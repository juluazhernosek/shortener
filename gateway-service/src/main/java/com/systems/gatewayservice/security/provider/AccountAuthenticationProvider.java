package com.systems.gatewayservice.security.provider;

import com.systems.gatewayservice.client.AccountClient;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class AccountAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AccountClient accountClient;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            accountClient.currentAccount("Basic " +
                    Base64.getEncoder().encodeToString((authentication.getName() + ":" + authentication.getCredentials()).getBytes()));
            return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(), authentication.getAuthorities());
        } catch (FeignException ex) {
            throw new InternalAuthenticationServiceException(String.valueOf(ex.status()));
        }

    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UsernamePasswordAuthenticationToken.class == aClass;
    }
}
