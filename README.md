# README #

#### Account service
Contains general user input logic: authentication and opening new account

Method	| Path	| Description	| User authenticated	| Available from UI
------------- | ------------------------- | ------------- |:-------------:|:----------------:|
GET	| /account/current	| Get specified account data for current user	| + | -	
POST	| /account/	| Open new account	| - | +


#### Statistics service
Performs calculations on major statistics parameters and captures each redirect for short link

Method	| Path	| Description	| User authenticated	| Available from UI
------------- | ------------------------- | ------------- |:-------------:|:----------------:|
GET	| /statistics/{account}	| Get specified account statistics	          | + | + 	


#### Link service
Stores info for each registered link: 1) Long url 2) Redirect code 3) Short url

Method	| Path	| Description	| User authenticated	| Available from UI
------------- | ------------------------- | ------------- |:-------------:|:----------------:|
POST	| /register/	| Register new link	| + | +	



#### Redirect service
Perform redirect from short url to long url

Method	| Path	| Description	| User authenticated	| Available from UI
------------- | ------------------------- | ------------- |:-------------:|:----------------:|
GET	| /{link}	| Redirect to long url by {link}	| - | +	

## How to run all the things?

Keep in mind, that you are going to start 7 Spring Boot applications, 1 MongoDB instance, 2 Redis instance and RabbitMq. Make sure you have 4 Gb RAM available on your machine

#### Before you start
- Install Docker and Docker Compose.

#### Development mode
If you'd like to build images yourself (with some changes in the code, for example), you have to clone all repository and build artifacts with maven, just run `mvn clean install`. Then, run `docker-compose -f docker-compose.yml -f docker-compose.dev.yml up`

Wait some seconds for registration all services in eureka


#### Important endpoints
- http://{docker_host}:80 - Gateway
- http://{dcoker_host}:8761 - Eureka Dashboard